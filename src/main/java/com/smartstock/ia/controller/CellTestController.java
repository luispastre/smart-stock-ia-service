package com.smartstock.ia.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.smartstock.ia.adapter.TestDto;
import com.smartstock.ia.adapter.TestDto2;
import com.smartstock.ia.adapter.TestDto3;
import com.smartstock.ia.adapter.TestDto4;
import com.smartstock.ia.service.LoadService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/test")
public class CellTestController {

	
	@Autowired
	private LoadService loadService;
	
	@PostMapping(value = "sessao", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> cellTest(@RequestBody TestDto dto) throws Exception {
		try {
			Map<String, String> map = new HashMap();
			map.put("id", "123");

			return ResponseEntity.ok(map);
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@PostMapping(value = "{id}/booking", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> cellTest(@RequestHeader("user_id") String user_id, @PathVariable("id") Long id,
			@RequestBody TestDto3 dto) throws Exception {
		try {
			System.out.println(user_id);
			System.out.println(id);
			System.out.println(dto);

			return ResponseEntity.ok().build();
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@GetMapping(value = "spots", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> cellTest2(@RequestParam("tech") String tech) throws Exception {
		try {

			List<TestDto2> techList = new ArrayList<>();

			techList.add(new TestDto2(1L, "React", "https://reactnative.dev/docs/assets/p_cat2.png", "JOJO", 2.32));
			techList.add(new TestDto2(2L, "Java",
					"https://www.tekla.com/sites/default/files/styles/medium_2x/public/media/2020-tms-time-is-money-icon.png?itok=c7UukJnl",
					"JAVINHA", 123.32));
			techList.add(new TestDto2(3L, "Python",
					"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQbHNwkEJu9u1hgibqEfP_TKXbpRfkT0fOHQw&usqp=CAU",
					"Serpente", null));
			techList.add(new TestDto2(4L, "Ruby",
					"https://i.pinimg.com/474x/14/ff/4f/14ff4fbe9f82b9e1f6c87a754cf281c4.jpg", "Rubens", null));
			techList.add(new TestDto2(5L, ".Net",
					"https://www.vippng.com/png/detail/38-386318_circular-facebook-icon-icons-pinterest-png-circular-photography.png",
					"DOTNERT", 90.0));

			return ResponseEntity.ok(techList);
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@GetMapping(value = "/dashboard", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> cellDashboard(@RequestHeader("user_id") String user_id) throws Exception {
		try {
			System.out.println(user_id);
			List<TestDto2> techList = new ArrayList<>();

			techList.add(new TestDto2(1L, "React", "https://reactnative.dev/docs/assets/p_cat2.png", "JOJO", 2.32));
			techList.add(new TestDto2(2L, "Java",
					"https://www.tekla.com/sites/default/files/styles/medium_2x/public/media/2020-tms-time-is-money-icon.png?itok=c7UukJnl",
					"JAVINHA", 123.32));
			techList.add(new TestDto2(3L, "Python",
					"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQbHNwkEJu9u1hgibqEfP_TKXbpRfkT0fOHQw&usqp=CAU",
					"Serpente", null));
			techList.add(new TestDto2(4L, "Ruby",
					"https://i.pinimg.com/474x/14/ff/4f/14ff4fbe9f82b9e1f6c87a754cf281c4.jpg", "Rubens", null));
			techList.add(new TestDto2(5L, ".Net",
					"https://www.vippng.com/png/detail/38-386318_circular-facebook-icon-icons-pinterest-png-circular-photography.png",
					"DOTNERT", 90.0));

			return ResponseEntity.ok(techList);

		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@PostMapping(value = "/store", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> cellStore(@RequestHeader("user_id") String user_id, @RequestBody TestDto2 dto)
			throws Exception {
		try {
			System.out.println(user_id);
			System.out.println(dto);

			return ResponseEntity.ok().build();
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@PostMapping(value = "/new", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> cellNew(@RequestHeader("user_id") String user_id, @ModelAttribute TestDto4 dto)
			throws Exception {
		try {
			System.out.println(user_id);
			System.out.println(dto);

			
			
			loadService.load(dto.getThumbnail());
			
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}
}
