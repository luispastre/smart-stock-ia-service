package com.smartstock.ia.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.smartstock.ia.adapter.ImportHistoryDto;
import com.smartstock.ia.adapter.SimulatePriceDto;
import com.smartstock.ia.dto.FilesDTO;
import com.smartstock.ia.entity.StockEntity;
import com.smartstock.ia.service.LoadService;
import com.smartstock.ia.service.StockService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/data")
@Api( value = "Data", tags = { "Data" })
public class DataController {

	@Autowired
	private LoadService loadService;
	@Autowired
	private StockService stockService;

	/**
	 * Retorna uma lista com todos os Fabricantes de Pneus cadastrados
	 */
	@ApiOperation(
			value = "Retorna uma lista com todos os Fabricantes de Pneus cadastrados.",
			notes = "Retorna uma lista com todos os Fabricantes de Pneus cadastrados.",
			tags = {"Search Tire Vendors"})
	
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public void uploadFile(){
		loadService.load();
	}
	
	
	@PostMapping(path = {"/upload-file"}, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public void uploadFile(@RequestPart("object") FilesDTO collectionDTO, @RequestPart("file") MultipartFile file) {
		try {		
						
			loadService.load(file);
			
		}catch (Exception e) {
		}			
	}


	@PostMapping(value = "/importHistory", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> cellNew(@ModelAttribute ImportHistoryDto dto)
			throws Exception {
		try {
			
			loadService.load(dto.getFile());
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}
	

	
	@GetMapping(value="/{stock}/{qtRows}")
	public ResponseEntity<?> getStockPrices(@PathVariable("stock") String stock, @PathVariable("qtRows") Integer qtRows) {
		try {
			
			StockEntity stockEntity = stockService.findStock(stock);
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Map<String, InnerClass> maps = new HashMap<>();
			stockEntity.getListHistoricPrice().forEach(item -> {
				InnerClass inner = maps.get(sdf.format(item.getIncludeDate()));
				if(inner == null) {
					inner = new InnerClass();
					inner.setOrder(item.getIncludeDate());
					inner.setDate(sdf.format(item.getIncludeDate()));
					maps.put(sdf.format(item.getIncludeDate()), inner);
				}
				inner.setRealPrice(item.getClosePrice());
				/*
				if(inner.getPredictPrice() == null) {
					inner.setPredictPrice(0.0);
				}
				*/
			});
			
			stockEntity.getListPredictedPrice().stream().forEach(item -> {
				InnerClass inner = maps.get(sdf.format(item.getIncludeDate()));
				if(inner == null) {
					inner = new InnerClass();
					inner.setOrder(item.getIncludeDate());
					inner.setDate(sdf.format(item.getIncludeDate()));
					maps.put(sdf.format(item.getIncludeDate()), inner);
				}
				inner.setPredictPrice(item.getClosePrice());
				/*
				if(inner.getRealPrice() == null) {
					inner.setRealPrice(0.0);
				}
				*/

			});
			
			List<InnerClass> values = new ArrayList<InnerClass>(maps.values());
			
			values.sort(new Comparator<InnerClass>() {
			    @Override
			    public int compare(InnerClass obj1, InnerClass obj2) {
			       return obj1.getOrder().compareTo(obj2.getOrder());
			    }
			});
			
			
			
			return ResponseEntity.ok(values.subList(values.size() -qtRows, values.size()));
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}

	}
	
	
	@GetMapping(value="/listStocks")
	public ResponseEntity<?> getStockPrices() {
		
		List<StockEntity> listStock = stockService.findAll();
		
		return ResponseEntity.ok(listStock);
	}	


	@PostMapping(value="/simulatePrice")
	public ResponseEntity<?> simulatePrice(@RequestBody SimulatePriceDto simulatePriceDto) {
		
		String decisao = stockService.simulatePrice(simulatePriceDto);
		
		return ResponseEntity.ok(decisao);
	}	


	private @Data class InnerClass {
		private Date order;
    	private String date;
    	private Double realPrice;
    	private Double predictPrice;
    	
    }
	

}


