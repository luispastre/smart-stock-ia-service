package com.smartstock.ia.service;

import java.util.List;

import com.smartstock.ia.adapter.SimulatePriceDto;
import com.smartstock.ia.entity.StockEntity;

public interface StockService {

	public void save(StockEntity stock);
	public StockEntity findStock(String symbol);
	public List<StockEntity> findAll();
	public String simulatePrice(SimulatePriceDto simulatePriceDto);
	
	
}
