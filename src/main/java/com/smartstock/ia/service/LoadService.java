package com.smartstock.ia.service;

import org.springframework.web.multipart.MultipartFile;

public interface LoadService {
	public void load();
	public void load(MultipartFile file);
	
}
