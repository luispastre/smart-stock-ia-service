package com.smartstock.ia.service.impl;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.opencsv.CSVReader;
import com.smartstock.ia.entity.StockEntity;
import com.smartstock.ia.entity.StockHistoricPriceEntity;
import com.smartstock.ia.service.LoadService;
import com.smartstock.ia.service.StockService;

@Service
public class LoadServiceImpl implements LoadService {

	@Autowired
	private StockService stockService;

	public static void main(String[] args) {
		new LoadServiceImpl().load();
	}

	public void load(MultipartFile file) {

		try (

			InputStream is = file.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			//var fileName = "src/main/resources/price-stock.csv";

			//try (var fr = new FileReader(fileName, StandardCharsets.UTF_8); 
					
				var reader = new CSVReader(br)) {

				String[] nextLine;
				// pula o cabeçalho
				reader.readNext();
				while ((nextLine = reader.readNext()) != null) {
					var row = nextLine[0].split(";");

					// for (var e : nextLine) {
					// var row = e.split(";");

					var date = new SimpleDateFormat("MM/dd/yyyy").parse(row[0]);

					var stock = stockService.findStock(row[1]);

					if (stock == null) {
						stock = new StockEntity();
						stock.setSymbol(row[1]);
						stock.setListHistoricPrice(new ArrayList<>());
					}

					Optional<StockHistoricPriceEntity> optional = stock.getListHistoricPrice().stream()
							.filter(hp -> hp.getIncludeDate().equals(date)).findFirst();
					var stockHistoric = new StockHistoricPriceEntity();
					if (!optional.isEmpty()) {
						stockHistoric = optional.get();
					}
					stockHistoric.setIncludeDate(date);
					stockHistoric.setOpenPrice(Double.valueOf(row[2]));
					stockHistoric.setHighPrice(Double.valueOf(row[3]));
					stockHistoric.setLowPrice(Double.valueOf(row[4]));
					stockHistoric.setClosePrice(Double.valueOf(row[5]));
					stockHistoric.setVolumeTmp(Double.valueOf(row[6]));

					stockHistoric.setStock(stock);
					stock.getListHistoricPrice().add(stockHistoric);

					stockService.save(stock);

				}
				// }
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void load() {
		// TODO Auto-generated method stub
		
	}

}
