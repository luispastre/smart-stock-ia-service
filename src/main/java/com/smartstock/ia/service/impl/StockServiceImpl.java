package com.smartstock.ia.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.smartstock.ia.adapter.SimulatePriceDto;
import com.smartstock.ia.entity.StockEntity;
import com.smartstock.ia.entity.StockPredictedPriceEntity;
import com.smartstock.ia.repository.PredictedStockRepository;
import com.smartstock.ia.repository.StockRepository;
import com.smartstock.ia.service.StockService;

@Service
public class StockServiceImpl implements StockService{

	@Autowired
	private StockRepository stockRepository;
	
	@Autowired
	private PredictedStockRepository predictedStockRepository;
	
	public void save(StockEntity stock) {
		stockRepository.save(stock);
	}
	

	public StockEntity findStock(String symbol) {

		List<StockEntity> listStock = 
						stockRepository.findStock(symbol);
		
		if(listStock.isEmpty())
			return null;
		else
			return listStock.get(0);
	}
	
	public List<StockEntity> findAll(){
		return stockRepository.findAll();
	}
	
	
	public String simulatePrice(SimulatePriceDto simulatePriceDto) {
		
		List<StockPredictedPriceEntity> stockPredictedPriceList = 
						predictedStockRepository.findStockPredictedPrice(simulatePriceDto.getStockId(), 
																Sort.by(Sort.Direction.DESC, "includeDate"));
		
		StockPredictedPriceEntity predictedPrice = stockPredictedPriceList.get(0);
		
		
		
		Double valor = predictedPrice.getClosePrice() * 0.05;
		
		
		if(simulatePriceDto.getPrice() > predictedPrice.getClosePrice() + valor) {
			return "COMPRAR";
		}
				
				
				
		if(simulatePriceDto.getPrice() < predictedPrice.getClosePrice() - valor) {
			return "VENDER";
		}
			
		
		return "";
	}
	
}
