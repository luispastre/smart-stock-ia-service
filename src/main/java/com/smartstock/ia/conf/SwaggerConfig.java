package com.smartstock.ia.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * http://localhost:8080/api/smart-stock-ia-service/swagger-ui.html
 * @author lpastre
 *
 */

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
    @Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          //.apis(RequestHandlerSelectors.any())
          .apis(RequestHandlerSelectors.basePackage("com.smartstock.ia"))
          .paths(PathSelectors.any())                          
          .build()
          .apiInfo(metaData());
    }

    
    private ApiInfo metaData() {
        return new ApiInfoBuilder()
                .title("Smart Stock IA Service REST API")
                .description("\"Smart Stock IA Service REST API for Online Store\"")
                .version("1.0.0")
                .license("Apache License Version 2.0")
                .licenseUrl("https://www.apache.org/licenses/LICENSE-2.0\"")
                .contact(new Contact("Pastre", "https://www.com.br/", "lpastre@gmail.com"))
                .build();
    }
    
    

}
