package com.smartstock.ia.adapter;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = true)
public @Data @ToString class TestDto3 {

	private String date;	
	
	
}
