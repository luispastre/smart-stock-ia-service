package com.smartstock.ia.adapter;

import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = true)
public @Data @ToString class ImportHistoryDto {

	private MultipartFile file;
	
}
