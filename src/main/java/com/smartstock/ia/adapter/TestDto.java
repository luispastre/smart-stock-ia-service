package com.smartstock.ia.adapter;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
public @Data class TestDto {

	private String email;	
	private String techs;	
	
}
