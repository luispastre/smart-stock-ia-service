package com.smartstock.ia.adapter;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
public @Data @AllArgsConstructor class TestDto2 {

	private Long id;	
	private String tech;
	private String url;
	private String company;
	private Double price;
	
	
	
}
