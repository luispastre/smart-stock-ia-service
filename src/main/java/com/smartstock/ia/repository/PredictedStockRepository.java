package com.smartstock.ia.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.smartstock.ia.entity.StockPredictedPriceEntity;

@Repository
public interface PredictedStockRepository extends JpaRepository<StockPredictedPriceEntity, Long>{

	
	@Query("SELECT s FROM StockPredictedPriceEntity s		  "
		    + "	  WHERE s.stock.id = ?1 						  ")
	public List<StockPredictedPriceEntity> findStockPredictedPrice(Long stockId, Sort sort);
	
}
