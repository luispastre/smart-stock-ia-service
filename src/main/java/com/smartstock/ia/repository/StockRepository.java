package com.smartstock.ia.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.smartstock.ia.entity.StockEntity;

@Repository
public interface StockRepository extends JpaRepository<StockEntity, Long>{

	
	@Query("SELECT s FROM StockEntity s						  "
			+ "		INNER JOIN s.listHistoricPrice sh		  "
			+ "		LEFT JOIN s.listPredictedPrice sp		      "
		    + "	  WHERE s.symbol = ?1 						  ")
	public List<StockEntity> findStock(String symbol);
	
}
