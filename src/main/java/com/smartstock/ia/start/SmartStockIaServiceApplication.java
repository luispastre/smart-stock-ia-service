package com.smartstock.ia.start;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.smartstock.ia"})
public class SmartStockIaServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmartStockIaServiceApplication.class, args);
	}

}
